CLIENTE DE NOTIFICACIONES ELECTRONICAS
===========================

Un cliente para utilizar el API REST implementado en la plataforma de notificaciones electrónicas.

- Autor: Manuel Henry Sanchez Carvajal <msanchez@agetic.gob.bo> Licencia: [LPG Bolivia v.1](LICENCIA.md)
- [Instalación](INSTALL.md)

## Modo de uso
 1. Se importa el modulo cliente

```sh
const ClienteNotificaciones = require('app-notificaciones');
```
 2. Se crea una instancia del cliente. IMPORTANTE: el constructor requiere el token de acceso y la ruta al servidor de Notificaciones Electrónicas

```sh
let clienteNotificaciones = new ClienteNotificaciones(token, urlServidor);
```

 3. Para enviar correos electrónicos se llama al método **correo**, con los  siguientes parámetros requeridos:
 
```sh
clienteNotificaciones.correo({
    "para": ["correodestino1@mail.com", ...],
    "cc": ["cc1@mail.com","cc2@mail.com", ...],
    "asunto": "asunto del mensaje",
    "contenido": "texto del mensaje",
    "adjunto": "ruta absoluta al archivo adjunto"
})
.then(response => {
    if(response.finalizado) {
        console.log('respuesta de servicio');
        console.log(response.mensaje);
    }
})
.catch(error => {
    console.log('error en servicio');
    console.log(error);
});
```

 4. Para enviar mensajes por SMS se llama al método **sms**, con los  siguientes parámetros requeridos:

```sh
clienteNotificaciones.sms({
    "para": ["77711111", ...],
    "contenido": "texto del mensaje"
})
.then(response => {
    if(response.finalizado) {
        console.log('respuesta de servicio');
        console.log(response.mensaje);
    }
})
.catch(error => {
    console.log('error en servicio');
    console.log(error);
});
```

 5. Para llamadas de voz, se llama al método **llamada**, con los  siguientes parámetros requeridos:

```sh
clienteNotificaciones.llamada({
    "numero": "77711111",
    "codigo": "12345"
})
.then(response => {
    if(response.finalizado) {
        console.log('respuesta de servicio');
        console.log(response.mensaje);
    }
})
.catch(error => {
    console.log('error en servicio');
    console.log(error);
});
```
el sistema genera  un archivo de audio con el código proporcionado y realiza la llamada, si se contesta se reproduce el audio generado.

6. Para mensajes push, se llama al método **push**, con los  siguientes parámetros requeridos:

```sh
clienteNotificaciones.push({
    "para": ["canal1", "canal2", .....],
    "contenido": "mensaje de notificacion"
})
.then(response => {
    if(response.finalizado) {
        console.log('respuesta de servicio');
        console.log(response.mensaje);
    }
})
.catch(error => {
    console.log('error en servicio');
    console.log(error);
});
```
el sistema genera  un archivo de audio con el código proporcionado y realiza la llamada, si se contesta se reproduce el audio generado.


## Tests
```sh
npm test
```

## Por hacer

Limpiar credenciales y tokens del historial de commits (probablemente será mejor eliminar `.git/` y subirlo limpio)
