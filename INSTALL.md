## Requisitos
```sh
Node 6 y superiores
```

## Instalación
La instalación del cliente se debe realizar desde el backend del sistema que utilizará notificaciones electrónicas.

TODO: quitar esto cuando este en el RESL
Para instalar este paquete tiene que tener su llave pública de su equipo cat ~/.ssh/id_rsa.pub registrado en https://gitlab.geo.gob.bo 

```sh
# TODO: cambiar la direccion del repo a la direccion del repo en el RESL
npm install git+ssh://git@gitlab.geo.gob.bo:agetic/app-notificaciones.git --save
```
Nota.- Si la instalación del paquete no avanza, agregar manualmente la llave privada de tu equipo a la conexión ssh:

```sh
# Agregando llave privada
ssh-add ~/.ssh/id_rsa
```
