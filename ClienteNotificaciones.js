'use strict';
const axios = require('axios');
const fs = require('fs');
const mime = require('mime');
const path = require('path');
const asyncReq = require('asyncawait/async');
const awaitReq = require('asyncawait/await');

function ClienteNotificaciones(token, baseUrl) {
    this.token = token;
    this.baseUrl = baseUrl;
    
    if(!this.token) {
        throw new Error('se requiere un token de acceso');
    }
    if(!this.baseUrl) {
        throw new Error('se requiere la ruta de acceso al servidor');
    }
    this.baseUrl += this.baseUrl.charAt(this.baseUrl.length - 1)!='/'?'/':'';
    this.correoApi = `${this.baseUrl}correo`;
    this.smsApi = `${this.baseUrl}sms`;
    this.llamadaApi = `${this.baseUrl}llamada`;
    this.pushApi = `${this.baseUrl}push`;
    this.estadoApi = `${this.baseUrl}correo/estado`;    // se toma como referencia el estado del microservicio de correo
    
    const settings = {
        "headers": {
            "Authorization": `Bearer ${this.token}`
        }
    };
    this.DataService = axios.create(settings);
}

/**
 * Para cualquier peticion se verifica si el servidor esta activo
 */
ClienteNotificaciones.prototype.verificaStatus = asyncReq (function () {
    return awaitReq (this.DataService.get(this.estadoApi));
});

/**
 * Método que realiza petición para envío de correo electrónico
 * @param {Object} params 
 */
ClienteNotificaciones.prototype.correo = asyncReq (function (params) {
    if(!params) {
        //throw new Error('Debe proporcionar parámetros');
        return new Promise((resolve, reject) => {
            reject("No se proporcionaron parámetros requeridos");
        });
    }

    // verificando conexion
    let status = awaitReq (this.verificaStatus());

    if(status && status.data && status.data == 'OK') {
        if(params.adjunto) {
            let archivo = params.adjunto.split('/');
            params.adjunto = {
                "nombre": archivo[archivo.length-1],
                "archivo": base64_encode(params.adjunto, '')
            };
        }
        if(params.adjuntoBase64) {
            let mimeType = params.adjuntoBase64.split(';')[0];
            mimeType = mimeType.split(':')[1];
            params.adjunto = {
                "nombre": "archivo_adjunto." + mime.getExtension(mimeType),
                "archivo": params.adjuntoBase64
            };
            delete params.adjuntoBase64;
        }
        
        try {
            let envioCorreo = awaitReq (this.DataService.post(this.correoApi, params));

            if(envioCorreo.status == 201) {
                return envioCorreo.data;
            }
            else {
                return new Promise((resolve, reject) => {
                    reject("Ocurrio un error en el envío");
                });
            }
        } catch (e) {
            console.log('ERROR CORREO ELECTRONICO');
            console.log(e);
            return {
                finalizado: false,
                mensaje: 'ocurrió un error en la petición'
            };
        }
    }
    else {
        return new Promise((resolve, reject) => {
            reject("No hay conexion con el servidor");
        });
    }
});

/**
 * Método que realiza petición para envío de SMS's
 * @param {Object} params 
 */
ClienteNotificaciones.prototype.sms = asyncReq (function (params) {
    if(!params || !params.para || !params.contenido) {
        //throw new Error('Debe proporcionar parámetros');
        return new Promise((resolve, reject) => {
            reject("No se proporcionaron parámetros requeridos");
        });
    }

    // verificando conexion
    // let status = await this.verificaStatus();

    // if(status && status.data && status.data == 'OK') {        
        try {
            let envioSms = awaitReq (this.DataService.post(this.smsApi, params));

            if(envioSms.status == 201) {
                return envioSms.data;
            }
            else if(envioSms.status == 401) {
                return envioSms.data;
            }
            else {
                return new Promise((resolve, reject) => {
                    reject("Ocurrio un error en el envío");
                });
            }
        } catch (e) {
            console.log('ERROR SMS');
            console.log(e);
            return {
                finalizado: false,
                mensaje: 'ocurrió un error en la petición'
            };
        }
    // }
    // else {
    //     return new Promise((resolve, reject) => {
    //         reject("No hay conexion con el servidor");
    //     });
    // }
});

/**
 * Método que realiza notificación de llamada de voz
 * @param {Object} params 
 */
ClienteNotificaciones.prototype.llamada = asyncReq (function (params) {
    if(!params || !params.numero || !params.codigo) {
        return new Promise((resolve, reject) => {
            reject("No se proporcionaron parámetros requeridos");
        });
    }

    params.para = params.numero;
    params.contenido = params.codigo;
    delete params.numero;
    delete params.codigo;

    try {
        let response = awaitReq (this.DataService.post(this.llamadaApi, params));
        if(response.status == 201) {
            return response.data;
        }
        else {
            return {
                finalizado: false,
                mensaje: 'ocurrió un error en la petición'
            };
        }
    } catch(e) {
        console.log('ERROR LLAMADA');
        console.log(e);
        return {
            finalizado: false,
            mensaje: 'ocurrió un error en la petición'
        };
    }
});

/**
 * Método que realiza notificacion PUSH a través de una aplicación móvil
 * @param {Object} params 
 */
ClienteNotificaciones.prototype.push = asyncReq (function (params) {
    if(!params || !params.para || !params.contenido) {
        //throw new Error('Debe proporcionar parámetros');
        return new Promise((resolve, reject) => {
            reject("No se proporcionaron parámetros requeridos");
        });
    }
    try {
        let request = awaitReq (this.DataService.post(this.pushApi, params));
        if(request.status == 201) {
            return request.data;
        }
        else {
            return new Promise((resolve, reject) => {
                reject("Ocurrio un error en el envío");
            });
        }
    } catch (e) {
        console.log('ERROR MENSAJE PUSH');
        console.log(e);
        return {
            finalizado: false,
            mensaje: 'ocurrió un error en la petición'
        };
    }
});

function base64_encode(file, type) {
    let filePath = path.resolve(file);

    var bitmap = fs.readFileSync(file);
    let mimeType = mime.getType(filePath);

    return 'data:' + (type!=''?type:mimeType) + ';base64,' + new Buffer(bitmap).toString('base64');
}

module.exports = ClienteNotificaciones;
